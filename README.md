# LICENSE
GNU GPL V3\
as always <3

# ABOUT Tetyga
Tetyga is a **te**rminal **ty**ping **ga**me designed to train the player
to type, first accurately, then quickly, without copying on screen text.
The game is written in python using curses and is XDG compliant,
so if you run a libre OS it should work exactly as expected.

I am learning a new keyboard layout and I have found that I can not type 
decently unless I am looking at the text on screen, because in the training
program I had been using this was always the case. I had inadvertently learned
to press the key of a letter I am looking at, instead of learning to type what
is on my mind. Further, I struggle tremendously with key-press accuracy when I'm
typing from my mind. I knew I needed to dedicate a couple days into programming a
game to help me invest in my typing ability, and once I had, I knew I had to share
it with the you.

To solve the problem of being unable to type from one's mind, the game does not show
the typing challenge while the challenge is being played. And to help focus on 
accuracy first, playing it right is worth a lot more then playing it fast when
it comes to the score. The game uses encouraging language to help motivate the
player; and it tells them when they've done better in a round than in previous rounds
to stimulate reward pathways when they are showing short term improvement.
Hopefully these measures make the game fun and rewarding while helping the player
to develop their typing skill. 



# INSTALL
to install run this command:\
`curl https://gitlab.com/Joe_0237/tetyga/-/raw/main/install_script | python`\
and to uninstall:\
`curl https://gitlab.com/Joe_0237/tetyga/-/raw/main/install_script | python - u`\
obviously you need `curl` and `python` for this to work, but you probably already have them

# HOW TO PLAY
In a terminal run `tetyga`\
A challenge will appear, remember it as you will have to type it on the next screen.\
Press `ENTER` when you are ready to start typing.\
When prompted, type out the challenge.\
Below your response you will see any corrections:\
`-` means you got this letter correct\
`_` means you should have typed a space\
`a` means you should have typed the letter 'a' etc.\
`<` means you typed too many letters\
You can use `BACKSPACE`, but you cannot otherwise move your cursor.\
As soon as you are correct the round is over, and you will see your round results.\
The results screen will ignore any key-presses for two seconds so you don't accidentally
skip it, after that, press any key to see your next challenge.\
To quit press `q` on the next challenge screen.\
To get to the next challenge screen from any screen, to get a new challenge or to quit, use
a keyboard-interrupt (press `ctrl`+`c`) 

## RESULTS
#### Best of *x*!
the last round was better than the previous *x* rounds
#### Score:
a number representing how well you did, bigger is better
#### Perfect: 
the score you would have gotten on the last round if
you made no mistakes but typed slowly the whole time
#### Mastery:
the best possible score for the last round, if every key was
pressed very shortly after the last and no mistakes were made 
